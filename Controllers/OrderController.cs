﻿using System;
using Microsoft.AspNetCore.Mvc;
using avlicenseapi.Database;
using avlicenseapi.Models;
using Microsoft.Extensions.Logging;
using System.Diagnostics.Tracing;

namespace avlicenseapi.Controllers
{
    public class OrderController : Controller
    {
        public IActionResult Index()
        {
            Console.WriteLine("Accessed Orders Index");
            return Json(new { api = "Order Controller", type = "CRUD", apiVersion = "0.1.0" });
        }

        public IActionResult Get() 
        {
            throw new System.NotImplementedException();
        }

        [HttpPost]
        public IActionResult Create(OrderModel order) 
        {
            Console.WriteLine("Created New Order " + order.ToString());
            String message = HydrogenDB.AddUpdateOrder(order);
            bool success = !message.Contains("Error:");
            Console.WriteLine(message);
            return Json(new { type = "Orders", success = success, message = message });
        }

        [HttpPost]
        public IActionResult LineItem(OrderLineItemModel lineItem)
        {
            Console.WriteLine("Created New Order Line Item " + lineItem.ToString());
            String message = HydrogenDB.InsertOrderLineItem(lineItem);
            bool success = !message.Contains("Error:");
            Console.WriteLine(message);
            return Json(new { type = "OrdersLineItem", success = success, message = message });
        }

        [HttpPost]
        public IActionResult Activation(OrderActivationModel activation)
        {
            Console.WriteLine("Created New Activation " + activation.ToString());
            String message = HydrogenDB.InsertOrderActivationCode(activation);
            bool success = !message.Contains("Error:");
            Console.WriteLine(message);
            return Json(new { type = "OrdersActivationCode", success = success, message = message });
        }

        [HttpPost]
        public IActionResult License(LicenseModel license)
        {
            Console.WriteLine("Update License " + license.ToString());
            String message = HydrogenDB.UpdateLicense(license);
            bool success = !message.Contains("Error:");
            Console.WriteLine(message);
            return Json(new { type = "UpdateLicense", success = success, message = message });
        }

        [HttpPut]
        public IActionResult Update(OrderModel order)
        {
            // Uses the same stored procedure
            return this.Create(order);
        }

        [HttpDelete]
        public IActionResult Delete()
        {
            throw new System.NotImplementedException();
        }
    }
}
