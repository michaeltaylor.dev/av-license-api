﻿using System;
using Microsoft.AspNetCore.Mvc;
using avlicenseapi.Database;

namespace avlicenseapi.Controllers
{
    public class DebugController : Controller
    {
        public IActionResult Index()
        {
            return Json(new { success = "Success", message = "Debug Route Test", apiVersion = "0.1.0" });
        }
        public IActionResult ConnectionTest() 
        {
            String message = HydrogenDB.TestConnection();
            bool success = !message.Contains("Error:");
            return Json(new { success = success, message = message });
        }
    }
}
