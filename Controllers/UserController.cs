﻿using System;
using Microsoft.AspNetCore.Mvc;
using avlicenseapi.Database;
using avlicenseapi.Models;

namespace avlicenseapi.Controllers
{
    public class UserController : Controller
    {
        public IActionResult Index()
        {
            return Json(new { api = "User Controller", type = "CRUD", apiVersion = "0.1.0" });
        }

        public IActionResult Get() 
        {
            throw new System.NotImplementedException();
        }

        [HttpPost]
        public IActionResult Create(UserModel user) 
        {
            Console.WriteLine("Created New user" + user.ToString());
            String message = HydrogenDB.AddUpdateUser(user);
            bool success = !message.Contains("Error:");
            Console.WriteLine(message);
            return Json(new { type = "Users", success = success, message = message });
        }

        [HttpPut]
        public IActionResult Update(UserModel user)
        {
            Console.WriteLine("Updated user " + user.ToString());
            String message = HydrogenDB.AddUpdateUser(user);
            bool success = !message.Contains("Error:");
            Console.WriteLine(message);
            return Json(new { type = "Users", success = success, message = message });
        }

        [HttpDelete]
        public IActionResult Delete()
        {
            throw new System.NotImplementedException();
        }
    }
}
