using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.IO;

namespace avlicenseapi.Models
{
    [DataContract]
    public class OrderModel
    {
        [DataMember]
        public int orderId { get; set; }
        [DataMember]
        public int encodedOrderId { get; set; }
        [DataMember]
        public String accountId { get; set; }
        [DataMember]
        public int billAmount { get; set; }
        [DataMember]
        public int deliveryCharge { get; set; }
        [DataMember]
        public int taxAmount { get; set; }
        [DataMember]
        public int paymentMethodId { get; set; }
        [DataMember]
        public DateTime orderDate { get; set; }
        [DataMember]
        public int processingStageId { get; set; }
        [DataMember]
        public String ipaddress { get; set; }    

        public override String ToString() {
            MemoryStream stream1 = new MemoryStream();  
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(OrderModel)); 
            ser.WriteObject(stream1, this);
            stream1.Position = 0;  
            StreamReader sr = new StreamReader(stream1);  
            return sr.ReadToEnd();  
        }    
    }
}