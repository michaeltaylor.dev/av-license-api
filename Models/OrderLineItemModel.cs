using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.IO;

namespace avlicenseapi.Models
{
    [DataContract]
    public class OrderLineItemModel
    {
        [DataMember]
        public int orderId { get; set; }
        [DataMember]
        public int originalProductId { get; set; }
        [DataMember]
        public String originalSku { get; set; }
        [DataMember]
        public int actualCost { get; set; }
        [DataMember]
        public int unitCost { get; set; }
        private static int _nonce = 0;
        [DataMember]
        public int nonce {
            get { 
                _nonce = _nonce + 1;
                return _nonce;
            }
            set { _nonce = value;}
        }

        public override String ToString() {
            MemoryStream stream1 = new MemoryStream();  
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(OrderLineItemModel)); 
            ser.WriteObject(stream1, this);
            stream1.Position = 0;  
            StreamReader sr = new StreamReader(stream1);  
            return sr.ReadToEnd();  
        }  
    }
}