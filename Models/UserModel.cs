using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.IO;

namespace avlicenseapi.Models
{
    [DataContract]
    public class UserModel
    {
        [DataMember]
        public String email { get; set; }
        [DataMember]
        public String passwordHash { get; set; }
        [DataMember]
        public String salt { get; set; }
        [DataMember]
        public String firstName { get; set; }
        [DataMember]
        public String lastName { get; set; }
        [DataMember]
        public String accountId { get; set; }
        
        public override String ToString() {
            MemoryStream stream1 = new MemoryStream();  
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(UserModel)); 
            ser.WriteObject(stream1, this);
            stream1.Position = 0;  
            StreamReader sr = new StreamReader(stream1);  
            return sr.ReadToEnd();  
        }         
    }
}