using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.IO;

namespace avlicenseapi.Models
{
    [DataContract]
    public class OrderActivationModel
    {
        [DataMember]
        public int orderId {get; set; }
        [DataMember]
        public int originalProductId { get; set; }
        [DataMember]
        public string activationCode { get; set; }

        public override String ToString() {
            MemoryStream stream1 = new MemoryStream();  
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(OrderActivationModel)); 
            ser.WriteObject(stream1, this);
            stream1.Position = 0;  
            StreamReader sr = new StreamReader(stream1);  
            return sr.ReadToEnd();  
        } 
    }
}