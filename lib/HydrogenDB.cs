using System;
using System.Data;
using System.Data.SqlClient;
using avlicenseapi.Models;

namespace avlicenseapi.Database
{
  public sealed class HydrogenDB 
  {

    /////////////////////////////////////////////////
    //////-------------Public API--------------//////
    /////////////////////////////////////////////////
    
    public static String AddUpdateUser(UserModel user)
    {
      return GetInstance()._UpsertUser(user);
    }

    public static String AddUpdateOrder(OrderModel order) 
    {
      return GetInstance()._UpsertOrder(order);
    }

    public static String InsertOrderLineItem(OrderLineItemModel lineItem)
    {
      return GetInstance()._InsertOrderLineItem(lineItem);
    }

    public static String InsertOrderActivationCode(OrderActivationModel activation)
    {
      return GetInstance()._InsertOrderActivationCode(activation);
    }

    public static String UpdateLicense(LicenseModel license)
    {
      return GetInstance()._UpdateLicense(license);
    }

    public static String TestConnection() 
    {
      return GetInstance()._TestConnection();
    }

    public static String DbStub(UserModel user) {
      return "Called Database Test function " + user.ToString();
    }
      
    /////////////////////////////////////////////////
    //////---------Instance Members------------//////
    /////////////////////////////////////////////////
    
#region Singleton
    private static HydrogenDB _singleton;    
    private HydrogenDB() { }
    
    private static HydrogenDB GetInstance()
    {
      if(_singleton == null)
      {
        _singleton = new HydrogenDB();
      }
      return _singleton;
    }
#endregion
    
#region Instance Properties
    private String host {
      get { return Environment.GetEnvironmentVariable("host"); }
    }

    private String port {
      get { return Environment.GetEnvironmentVariable("port"); }
    }

    private String password {
      get { return Environment.GetEnvironmentVariable("password"); }
    }

    private String username {
      get { return Environment.GetEnvironmentVariable("username"); }
    }

    private String connectionStr {
      get 
      {
        String dbTemplate = "Data Source={0},{1};Initial Catalog=Hydrogen_DB;User ID={2};Password={3}";
        return String.Format(dbTemplate, host, port, username, password);
      }
    }
#endregion
    
#region Instance Functions
    private String _TestConnection()
    {
      using (SqlConnection dbConnection = new SqlConnection(connectionStr)) 
      {
        try 
        {
          dbConnection.Open();
          if(dbConnection.State == ConnectionState.Open)
          {
            return "DB Connected";
          } else {
            return "Error: DB Not Connected";
          }
        } catch (Exception error) {
          return "Error: DB Not Connected";
        }
      }
    }
    private String _UpsertUser(UserModel user) {
      using (SqlConnection dbConnection = new SqlConnection(connectionStr)) 
      {
        using (SqlCommand cmd = new SqlCommand("dbo.UpsertUser", dbConnection)) 
        {
          cmd.CommandType = CommandType.StoredProcedure;

          cmd.Parameters.Add("@email", SqlDbType.VarChar, 255).Value = user.email;
          cmd.Parameters.Add("@passwordHash", SqlDbType.VarChar, 50).Value = user.passwordHash;
          cmd.Parameters.Add("@salt", SqlDbType.VarChar, 50).Value = user.salt;
          cmd.Parameters.Add("@firstName", SqlDbType.VarChar, 100).Value = user.firstName;
          cmd.Parameters.Add("@lastName", SqlDbType.VarChar, 100).Value = user.lastName;
          cmd.Parameters.Add("@accountID", SqlDbType.BigInt).Value = user.accountId;
          cmd.Parameters.Add("@isActive", SqlDbType.Bit).Value = 1;
          try {
            dbConnection.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            if (rowsAffected == 0) {
              return "Error: User already exists";
            } else {
              return "User added successfully";
            }
          } catch (Exception error) {
            return "Error: " + error.ToString();
          }
        }
      }
    }

    private String _UpsertOrder(OrderModel order)
    {
      using (SqlConnection dbConnection = new SqlConnection(connectionStr)) 
      {
        using (SqlCommand cmd = new SqlCommand("dbo.UpsertOrder", dbConnection)) 
        {
          cmd.CommandType = CommandType.StoredProcedure;
          
          cmd.Parameters.Add("@orderId", SqlDbType.Int).Value = order.orderId;
          cmd.Parameters.Add("@encodedOrderId", SqlDbType.Int).Value = order.encodedOrderId;
          cmd.Parameters.Add("@accountId", SqlDbType.VarChar, 50).Value = order.accountId;
          cmd.Parameters.Add("@billAmount", SqlDbType.Int).Value = order.billAmount;
          cmd.Parameters.Add("@deliveryCharge", SqlDbType.Int).Value = order.deliveryCharge;
          cmd.Parameters.Add("@taxAmount", SqlDbType.Int).Value = order.taxAmount;
          cmd.Parameters.Add("@paymentMethodId", SqlDbType.Int, 9).Value = order.paymentMethodId;
          cmd.Parameters.Add("@orderDate", SqlDbType.DateTime).Value = order.orderDate;
          cmd.Parameters.Add("@processingStageId", SqlDbType.Int, 3).Value = order.processingStageId;
          cmd.Parameters.Add("@ipaddress", SqlDbType.VarChar, 20).Value = order.ipaddress;
      
          try {
            dbConnection.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            if (rowsAffected == 0) {
              return "Error: Order already exists";
            } else {
              return "Order added successfully";
            }
          } catch (Exception error) {
            return "Error: " + error.ToString();
          }
        }
      }
    }

    private String _InsertOrderLineItem(OrderLineItemModel lineItem)
    {
      using (SqlConnection dbConnection = new SqlConnection(connectionStr)) 
      {
        using (SqlCommand cmd = new SqlCommand("dbo.InsertOrderLineItem", dbConnection)) 
        {
          cmd.CommandType = CommandType.StoredProcedure;
          
          cmd.Parameters.Add("@orderId", SqlDbType.Int).Value = lineItem.orderId;
          cmd.Parameters.Add("@originalProductId", SqlDbType.Int).Value = lineItem.originalProductId;
          cmd.Parameters.Add("@originalSku", SqlDbType.VarChar, 50).Value = lineItem.originalSku;
          cmd.Parameters.Add("@actualCost", SqlDbType.Int).Value = lineItem.actualCost;
          cmd.Parameters.Add("@unitCost", SqlDbType.Int).Value = lineItem.unitCost;
      
          try {
            dbConnection.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            if (rowsAffected == 0) {
              return "Error: Order Line Item already exists";
            } else {
              return "Order Line Item added successfully";
            }
          } catch (Exception error) {
            return "Error: " + error.ToString();
          }
        }
      }
    }

    public String _InsertOrderActivationCode(OrderActivationModel activation)
    {
      using (SqlConnection dbConnection = new SqlConnection(connectionStr)) 
      {
        using (SqlCommand cmd = new SqlCommand("dbo.InsertOrderActivationCode", dbConnection)) 
        {
          cmd.CommandType = CommandType.StoredProcedure;
          
          cmd.Parameters.Add("@orderId", SqlDbType.Int).Value = activation.orderId;
          cmd.Parameters.Add("@originalProductId", SqlDbType.Int).Value = activation.originalProductId;
          cmd.Parameters.Add("@activationCode", SqlDbType.VarChar, 64).Value = activation.activationCode;
      
          try {
            dbConnection.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            if (rowsAffected == 0) {
              return "Error: Activation code Line already exists";
            } else {
              return "Activation code added successfully";
            }
          } catch (Exception error) {
            return "Error: " + error.ToString();
          }
        }
      }
    }

    public String _UpdateLicense(LicenseModel license) 
    {
      using (SqlConnection dbConnection = new SqlConnection(connectionStr)) 
      {
        using (SqlCommand cmd = new SqlCommand("dbo.UpdateLicense", dbConnection)) 
        {
          cmd.CommandType = CommandType.StoredProcedure;
          
          if (license.orderId != null) {
            cmd.Parameters.Add("@orderId", SqlDbType.Int).Value = license.orderId;
          }

          if (license.startDate != null) {
            cmd.Parameters.Add("@startDate", SqlDbType.DateTime).Value = license.startDate;
          }

          if (license.endDate != null) {
            cmd.Parameters.Add("@endDate", SqlDbType.DateTime).Value = license.endDate;
          }

          if (license.simaltaneousUsers != null) {
            cmd.Parameters.Add("@simaltaneousUsers", SqlDbType.Int).Value = license.simaltaneousUsers;
          }

          if (license.maximumRegisteredUsers != null) {
            cmd.Parameters.Add("@maximumRegisteredUsers", SqlDbType.Int).Value = license.maximumRegisteredUsers;
          }

          if (license.active != null) {
            cmd.Parameters.Add("@active", SqlDbType.Bit).Value = license.active;
          }       
          
          if (license.licenseTypeId != null) {
            cmd.Parameters.Add("@licenseTypeId", SqlDbType.Int).Value = license.licenseTypeId;
          }

          if (license.activationCode != null) {
            cmd.Parameters.Add("@activationCode", SqlDbType.VarChar, 75).Value = license.activationCode;
          }   
          
          try {
            dbConnection.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            if (rowsAffected == 0) {
              return "Error: 0 rows affected";
            } else {
              return "License Updated successfully";
            }
          } catch (Exception error) {
            return "Error: " + error.ToString();
          }
        }
      }
    }
#endregion
  }
}