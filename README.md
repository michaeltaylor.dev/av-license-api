Creates an ASP.Net Server that serves this project to http://127.0.0.1:5000

## Run Locally
First make sure you are added to the docker team so you can push the image to the registry.

Second: create a .env file locally.

To build image locally run:

```
docker build -t tsaas/avlicenseapi:latest .
```

To push it to the repository run:
```
docker push tsaas/avlicenseapi:latest
```

Once the above is finished run the following to run locally:
```
docker-compose up
```

This commands forwards the internal 5000 port to 8080 so you should now be able to view this project at http://127.0.0.1:8080/

To shutdown the server run 
```
docker-compose down
```

## Secrets

Assume this image is public, dont include any secret information in the codebase.
Instead add a .env file to your project and any credentials or connection strings there. The docker file will load them into the image as ENV variables. You can access the ENV variables from the codebase.